#! /usr/bin/env bash
#SBATCH --exclusive
#SBATCH --constraint=bora
#SBATCH --nodes=4
#SBATCH --cpus-per-task=36
#SBATCH --output=test.4.out
#SBATCH --ntasks-per-node=1
#SBATCH --time=0:5:0

export STARPU_NCPU=1
export STARPU_MPI_STATS=1
export STARPU_FXT_TRACE=$1
echo STARPU_FXT_TRACE=$1
mkdir -p actual_traces
export STARPU_TRACE_BUFFER_SIZE=$((2*1024))
export STARPU_FXT_PREFIX=$(pwd)/actual_traces/
export STARPU_FXT_EVENTS="USER|TASK|TASK_VERBOSE|TASK_VERBOSE_EXTRA|DATA|DATA_VERBOSE|WORKER|WORKER_VERBOSE|DSM|DSM_VERBOSE|SCHED|SCHED_VERBOSE|LOCK|LOCK_VERBOSE|EVENT|EVENT_VERBOSE|MPI|MPI_VERBOSE|MPI_VERBOSE_EXTRA|HYP|HYP_VERBOSE"

x=4
y=4
p=2
q=2

x=3
y=4
p=1
q=2
#p=1
#q=1
S=2
b=5
s=2

debug="-debug"
debug=""
#export STARPU_DISABLE_KERNELS=1

function process() {
	echo == $out ==
	cat $out | grep seconds
	cat $out | grep nb_sends | sort
	cat $out | grep TOTAL | sort
}

mpirun -n $((p*q)) hostname
echo == classical ==
siter=6
siter=2
for iter in 2 4 6 8
#for iter in $siter 
do
	out=classical_${iter}.log
	mpirun -n $((p*q)) build/starpu_example_stencil $debug -iter $iter -X $x -Y $y -P $p -Q $q -S $S \
		-b $b -s $s &> $out
	process
done
echo == communicatoin-avoiding ==
for iter in 2 4 6 8
#for iter in $siter 
do
	out=replicating.${iter}.log
	#mpirun -debug --console \
	mpirun \
		-n $((p*q)) build/starpu_example_stencil $debug -iter $iter -X $x -Y $y -P $p -Q $q -S $S \
		 -b $b -s $s -replicating &> $out
	process
done
