\section{Introduction}
\label{sect:intro}

Recent trends in computer architectures led to machines where the computing power
is overprovisioned when compared to the memory speed. These trends have been
exacerbated as exascale machines are composed of many-cores CPUs often
accelerated with specialized hardware like GPUs. Such architecture makes it more
difficult to write applications in a simple manner while still benefiting from
the computing power at scale. In this context,
it is primordial that computer scientists offer high-level abstract programming
models that are capable of harnessing the capabilities of modern machines.

MPI~\cite{mpi:93} and OpenMP~\cite{o.a.r.b:13} have been for decades
and still are widely used programming APIs for writing parallel
applications even on modern supercomputers. Allow these provide direct
access to low-level hardware features and, consequently, allow for
highly tuned code optimizations, their use can be cumbersome and does
not allow for high productivity. Both performance and productivity are
important because being able to implement as well as maintain and
easily expand mathematical software is key to its durability: it is
impossible to engage with software development with a deep, precise,
extensive knowledge of every architectural detail.

In the past decade, runtime systems (often simply referred to as
\textit{runtimes}) have been explored as abstraction layers between
the evermore complex computer architecture and the users'
applications. A plethora of runtime systems have been designed with
different goals, architecture coverage and interfaces~\cite{t:18}.
Task-based runtime systems such as StarPU~\cite{a.t.n.w:11} or
PaRSEC~\cite{s.l.t.b.a:15} have shown tremendous performance while
only requiring the user to provide a Directed Acyclic Graph (DAG)
representation of their applications. Once the DAG has been described
by the programmer the corresponding tasks will be scheduled according
to the used machine configuration: this approach has yielded adequate
usage of modern {super-computers}. Although numerous well knwon
algorithms can be conveniently expressed through the programming
interfaces of the most widely-used runtimes, these seem to lack the
necessary features to implement some more advanced algorithms that
have been proposed in the literature. 


% While state-of-the-art
% general-purpose runtime systems offer programming interfaces with
% sufficient features to express numerous commonly used algorithms they don't
% cover all the algorithms that have been studied in the past. This lack
% of coverage stems from current limitations of task-based runtime
% systems (tasks' overhead, DAG size, ...)  ~\cite{y:12} which are still
% being explored.

% Data write replication as introduced in this paper is found in
% several communication-avoiding algorithms including
% %direct matrix factorization and
% iterative methods such as stencil computations.
% %§This feature is used to achieve greater
% %scalability compared to the traditional variants of these algorithms: they
% %essentially trade off an increased computational load with a reduction in
% %communication volume, improving bandwidth, or a decrease in the number of
% %messages, improving latency.
% %In its extended state, proposed in \cite{a.b.g.h.j:23},
% The STF
% programming model currently lacks idioms to describe such replication that
% is a strong requirement in the
% expression of these algorithms.
% %This section is dedicated to the specification
% %of the related features.

%Stencil algorithms are not easily captured in a task-based
%fashion.
%%Nonetheless when domain scientists study fields of
%%temperature, velocities, \emph{etc.}, they often rely on this type of algorithms
%%to explore the evolution of systems.
%Scalable variants of stencil algorithms
%have been designed in order to make the study of large,
%complex systems possible~\cite{d.h.m.y:08}.
%The core aspect of these variants is to trade-off bandwidth and
%floating-point operations as presented in Section~\ref{sec:back.stencil}.
%Such a trade-off can lead to an improved scalability
%as modern machines often overprovision floprate over bandwidth. While
%replication of floating-point operations is not difficult in itself, it poses
%a challenge when described to a runtime system as the multiple existing copies
%that are updated concurrently should be tracked in a simple, efficient manner.

In our recent work~\cite{a.b.c.e.f.f.g.j.p.p:23,a.b.g.h.j:23} we have
partially addressed this concern by proposing some extensions of the
sequential task flow (STF) programming model that allows for easily
expressing 2D and 3D algorithms for the parallel matrix multiplication
operation. In this work, we pursue this effort and focus on enabling
portable implementations of more complex algorithms that take advantage of
replicated, redundant computations to reduce the volume and number of
communications to improve scalability. More specifically, our
contribution amounts to an extension of the STF programming model and
of the underlying general-purpose runtime.

The remainder of this document is structured as follows.
A brief description of the STF programming model is given in
Section~\ref{sec:background}. In Section~\ref{sec:repl}, we propose a
novel access mode as part of the STF programming model that acts as a flag
enabling the replicating feature. 
%This section also presents an implementation for all-reduce operation. 
In order to demonstrate the ease of use and expressiveness of our
approach, we showcase our implementation of two different algorithms
in the following sections. Section~\ref{sec:repl.5pts} presents our
STF implementation of a communication-avoiding variant of the
five-points stencil algorithm to illustrate the performance and
elegance of the replicating mechanism. The resulting code indeed
benefits from a similar maintainability as the classical variant while
exhibiting better scalability up to 64 compute nodes.
%An additional extension of the STF programming model to enable allreduce
%reduction support make it possible to consider Cholesky decomposition as an
%additional demonstration of the capability of the programming model in
Section~\ref{sec:scal_chol} compares an advanced scalable variant of
the Cholesky decomposition with more traditional variants that do not
require neither redundant computations nor 3D logical process
grids. We present an experimental comparison with
ConfCHOX~\cite{k.l.j.v:21} which is a state-of-the-art library using
replication. The strong scalability benchmark is conducted on up to
256 compute nodes over relatively small problems that stress
communication and computation overlap.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "submission"
%%% TeX-command-extra-options: "-shell-escape --enable-write18 --synctex=1"
%%% jinx-languages: "en"
%%% End:
