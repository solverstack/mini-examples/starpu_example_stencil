x=3
y=6
p=1
q=2
S=1
b=4
s=4
iter=16

export STARPU_NCPU=1 
export STARPU_MPI_COOP_SENDS=0 
export STARPU_COMM_STATS=1 
guix shell --preserve=^STARPU_ --pure --verbosity=2 starpu-example-stencil \
	--with-git-url=starpu-example-stencil=$HOME/starpu_example_stencil \
	--with-git-url=starpu=$HOME/starpu/StarPU --with-branch=starpu=all_redux \
	--with-input=openmpi=nmad \
 	--with-git-url=nmad=$HOME/pm2 --with-git-url=puk=$HOME/pm2 --with-git-url=pioman=$HOME/pm2 --with-git-url=pukabi=$HOME/pm2 --with-git-url=padicotm=$HOME/pm2 \
 	--with-branch=nmad=master --with-branch=puk=master --with-branch=pioman=master --with-branch=pukabi=master --with-branch=padicotm=master \
	-- mpirun -np $((p*q)) -n $((p*q)) starpu_example_stencil -P $p -Q $q -X $x -Y $y -b $b -s $s -S $S -iter $iter
