(add-to-list 'load-path "./include/compose-publish/")
(require 'compose-publish)

(defun org-latex-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case nil
      (org-latex-publish-to-pdf
       plist filename pub-dir)
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (append-to-file (point-min) (point-max) "$(tty)")))))

(defun org-beamer-publish-to-pdf-verbose-on-error (plist filename pub-dir)
  (condition-case nil
      (org-beamer-publish-to-pdf
       plist filename pub-dir)
    (error
     (with-current-buffer "*Org PDF LaTeX Output*"
       (append-to-file (point-min) (point-max) "$(tty)")))))

(setq org-latex-pdf-process (list "latexmk --shell-escape -f -pdf -interaction=nonstopmode -output-directory=%o %f"))

(setq org-publish-project-alist
      (list
       (list "site-org"
             :base-directory "."
             :base-extension "org"
             :recursive nil
             :exclude "public"
             :publishing-function '(org-latex-publish-to-pdf-verbose-on-error org-html-publish-to-html)
             :publishing-directory "./public"
             :html-head-extra "<link rel=\"icon\" type=\"image/x-icon\" href=\"https://mfelsoci.gitlabpages.inria.fr/thesis/favicon.ico\"/>")
       (list "site-static"
             :base-directory "."
             :base-extension "png\\|csv\\|txt\\|pdf\\|svg\\|yml\\|el\\|setup"
             :recursive t
             :exclude "public"
             :publishing-function '(org-publish-attachment)
             :publishing-directory "./public/")
       (list "site" :components '("site-org" "site-static"))))

(provide 'qrm-study-cholesky)
