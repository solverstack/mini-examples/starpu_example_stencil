#! /usr/bin/env bash
#SBATCH --exclusive
#SBATCH --constraint=bora
#SBATCH --nodes=4
#SBATCH --cpus-per-task=36
#SBATCH --output=test.4.out
#SBATCH --ntasks-per-node=1
#SBATCH --time=0:5:0

export STARPU_NCPU=1
export STARPU_MPI_STATS=1

x=4
y=4
p=2
q=2

x=3
y=4
p=1
q=2
#p=1
#q=1
S=2
b=5
s=2

debug="-debug"
debug=""
#export STARPU_DISABLE_KERNELS=1

function process() {
	echo == $out ==
	cat $out | grep seconds
	cat $out | grep nb_sends | sort
	cat $out | grep TOTAL | sort
}

mpirun -n $((p*q)) hostname
echo == classical ==
#for iter in 2 4 6 8
siter=2
for iter in $siter 
do
	out=classical_${iter}.log
#	mpirun -debug --console \
#		-n $((p*q)) build/starpu_example_stencil $debug -iter $iter -X $x -Y $y -P $p -Q $q -S $S \
#		-b $b -s $s 
#	process
done
echo == communicatoin-avoiding ==
#for iter in 2 4 6 8
for iter in $siter 
do
	out=replicating.${iter}.log
	mpirun -debug --console \
		-n $((p*q)) build/starpu_example_stencil $debug -iter $iter -X $x -Y $y -P $p -Q $q -S $S \
		 -b $b -s $s -replicating 
	process
done
