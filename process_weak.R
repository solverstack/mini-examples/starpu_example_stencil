#!/usr/bin/env Rscript
library(plyr)
library(tidyverse)
library(ggplot2)
library(ggpubr)
library(stringr)

my.theme <- theme_bw() + 
            theme( text=element_text(size=20),
		   legend.key.width = unit(3, 'cm'),
		   legend.title=element_text(size=20),
		   legend.text=element_text(size=30),
		   #legend.direction="vertical",
		   legend.direction="horizontal",
		   legend.box="horizontal",
		   #legend.box="vertical",
		   legend.position = "bottom",
		   #legend.position = "right",
		   legend.margin = margin(),
		   axis.line = element_line(colour = "black"),
		   axis.text=element_text(size=20),
		   axis.title=element_text(size=25,face="bold"),
#                   axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1.0),
		   plot.title = element_text(size=30), 
		   strip.text.x = element_text(size = 20),
		   strip.text.y = element_text(size = 20),
                   panel.grid.major = element_line(colour="grey70"),
                   panel.grid.minor = element_line(colour="grey90"),
# make background transparent
		   panel.background = element_rect(fill = "transparent"),
		   plot.background = element_rect(fill = "transparent", color = NA), 
		   legend.background = element_rect(fill = "transparent"),
                   legend.key = element_rect(fill = "transparent", colour = NA),
)
source("my_theme.R")

# === FIXED
# b = 128 : too small
tbl_16 <- read.csv("rome.16.fixed.csv")
tbl_36 <- read.csv("rome.36.fixed.csv")
tbl_64 <- read.csv("rome.64.fixed.csv")
# b = 1024 : adequate ?
tbl_16 <- read.csv("rome.16.fixed.3.csv")
tbl_36 <- read.csv("rome.36.fixed.3.csv")
# fix ring_size
tbl_16 <- read.csv("rome.16.fixed.4.csv")
tbl_36 <- read.csv("rome.36.fixed.4.csv")
tbl <- rbind(tbl_16,tbl_36)
pdfname <- "weak.pdf"

# == STRONG
pdfname <- "strong.pdf"
# here rome is on a weak scalability regime
rome <- read.csv("csv/rome.4.strong.csv")
skylake <- read.csv("csv/skylake.4.csv")
print("???")
for (nodes in c(16,36,64)) {
	rome <- rbind(rome, read.csv(paste("csv/rome",nodes,"strong","csv",sep=".")))
	skylake <- rbind(skylake, read.csv(paste("csv/skylake",nodes,"csv",sep=".")))
	print("???")
}
rome$arch = "rome"
rome$ncpu = 128
skylake$arch = "skylake"
skylake$ncpu = 48
tbl <- rbind(rome,skylake)
print("===")
tbl <- subset(tbl, arch == "rome" & X == 144*1024 | arch =="skylake" & X == 64*1024)
tbl <- subset(tbl, cell_ns != 16)
tbl <- subset(tbl, s <= iter)
tbl <- subset(tbl, iter == 38)
print(tbl)

#tbl <- rbind(tbl,tbl_64)

tbl[tbl$algo=="classical",]$s = 1
tbl$grp <- with(tbl,interaction(X,Y,iter,b,s))

stats <- ddply(tbl,
               .(P,X,Y,iter,cell_ns,b,s,algo,arch,ncpu),
                   summarize, med_time=median(time),
                              min_time=min(time),
                              max_time=max(time))
print(stats)

simpleCap <- function(x) {
  s <- strsplit(x, " ")[[1]]
  paste(toupper(substring(s, 1,1)), substring(s, 2),
      sep="", collapse=" ")
}

my_labeller <- function(data) {
  print(data)
  labels = list()
  if ("arch" %in% colnames(data)) {
    lbl <- c()
    for (i in seq_len(nrow(data))) {
      lbl <- c(lbl,simpleCap(data$arch[[i]]))
    }
    labels[[1]] <- lbl
    lbl <- c()
    for (i in seq_len(nrow(data))) {
    lbl <- c(lbl, paste0(
		    "X = Y = ",
		    format(data$X[[i]]/data$b[[i]],big.mark=","),"*b with",
		    " b = ", data$b[[i]],
		    "\nover ", data$iter[[i]]," iterations")
		    )
    }
    labels[[2]] <- lbl
  }
  if ("cell_ns" %in% colnames(data)) {
#    B = 1024
#    data$label <- paste0(format(
#          data$cell_ns * B*B/1000/1000,big.mark=",",digits=3),
#          " ms per block")
    lbl <- paste0(format(
          9/data$cell_ns,big.mark=",",digits=1),
          " Gflop/s")
    lbl <- paste0(format(
          data$cell_ns,big.mark=",",digits=1),
          " ns per cell")
    labels[[1]] <- lbl
  }
  print(labels)
  labels
}

stats$med_floprate <- with(stats,iter*9*X*Y/med_time/10^9)
stats$max_floprate <- with(stats,iter*9*X*Y/max_time/10^9)
stats$min_floprate <- with(stats,iter*9*X*Y/min_time/10^9)
color_values <- c(`1`="#8a0608",`4`="#146e2c",`16`="#27c250",`38`="#1df557")
my.tbl <- stats#"[stats$arch == "skylake",]
plt <- ggplot(data = my.tbl,aes(x=P,y=100*med_floprate/(P*ncpu*9/cell_ns), #iter/med_time,
	fill=as.factor(s),color=as.factor(s))) +
	#geom_ribbon(aes(ymin=iter/min_time,ymax=iter/max_time),alpha=0.2) +
#	geom_ribbon(aes(ymin=min_floprate,ymax=max_floprate),alpha=0.2) +
#	geom_ribbon(aes(ymin=100*min_floprate/(P*ncpu*9/cell_ns),ymax=100*max_floprate/(P*ncpu*9/cell_ns)),alpha=0.2) +
	geom_line(linewidth=4) +
	geom_point(shape=21,stroke=3,size=8,fill="white") +
	geom_point(shape=21,stroke=3,size=8,fill=NA) +
	scale_y_continuous(guide=guide_axis(check.overlap=TRUE),
			   limits=c(0,NA),
                           labels = function(x) format(x,big.mark=",",scientific=FALSE),
                           #name="Throughput (iteration/s)") + #, trans="log")+
                           name="% of machine use") + #, trans="log")+
	scale_x_continuous(guide=guide_axis(check.overlap=TRUE),
                         labels = function(x) format(x,big.mark=",",scientific=FALSE),
			 breaks = unique(my.tbl$P),
                         name="# of nodes") + #, trans="log")+
	facet_grid(arch + X + Y + b + iter ~ cell_ns, scales="free_y", 
		labeller = my_labeller) + #, switch="x") +
	scale_shape(solid=FALSE) +
	scale_color_manual(name="s",
			   values=color_values)+
	scale_fill_manual( name="s",
			   values=color_values)+
	my.theme

ggsave(pdfname, plt, width=60, height=36, units="cm", device=cairo_pdf, bg="transparent")
