/* StarPU --- Runtime system for heterogeneous multicore architectures.
 *
 * Copyright (C) 2011-2022  Université de Bordeaux, CNRS (LaBRI UMR 5800), Inria
 *
 * StarPU is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) anx later version.
 *
 * StarPU is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the GNU Lesser General Public License in COPYING.LGPL for more details.
 */

#include <starpu.h>
#include <starpu_mpi.h>
#include <math.h>
#include <time.h>
#include <unistd.h>


// https://stackoverflow.com/questions/3557221/how-do-i-measure-time-in-c
double what_time_is_it()
{
    struct timespec now;
    clock_gettime(CLOCK_REALTIME, &now);
    return now.tv_sec + now.tv_nsec*1e-9;
}

int display = 0;
int niter = 10;
int b = 16; // each handle holds b*b cells
int s = 16; // we replicate the computation s cells away at most
int S = 2; // we save S grids
int wait_ns_per_cell = 10;
int X = 8;
int Y = 8;
int P = 2;
int Q = 2;
int debug = 0;
int prof = 0;
int trial = 1;

int replicated_variant = 0;


typedef struct Cells {
	float* center;
	float* north;
	float* south;
	float* east;
	float* west;
	float* nowe;
	float* sowe;
	float* noea;
	float* soea;
	int owner;
} Cell;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

void fill_cpu(void *descrs[], void *_args)
{
	float *xy   = (float *)STARPU_MATRIX_GET_PTR(descrs[0]);
	for (int i = 0; i < b; i++){
		for (int j = 0; j < b; j++){
			xy[i*b+j] = 0.0f; // could care less about how the mesh is filled
		}	
	}	
}

void stencil_cpu(void *descr[], void *_args)
{
	int current_ring,loop,x,y,wait_ns_per_cell,nb_handles;
	enum starpu_data_interface_id ids[6];
	starpu_codelet_unpack_args(_args, &current_ring, &loop, &x, &y, &wait_ns_per_cell,ids);
	int rank = starpu_mpi_world_rank();

//	fprintf(stdout, "VALUES: %2.2f %2.2f %2.2f %2.2f %2.2f\n", *xy, *xm1y, *xp1y, *xym1, *xyp1);

	struct timespec ts;
	ts.tv_sec = 0;

	/* 
 *		
 *	NW | N | NE
 *	---+---+--- 			
 *	 W | C | E
 *	---+---+--- 			
 *	SW | S | SE
 *
 * 	Assuming this codelet always update a domain that is coined "C", its owner should update w.r.t to all the
 * 	neighbouring domain whatever the value of the parameter of the replicating ring.
 *
 *	For other processes that will replicate the computation, assuming the parameter of the replication is r for the
 *	current task, they should only update part of the current sub-domain C.
 *
 *	When update C, the iteration goes as
 *	for i=ib,ie; for j=jb,je
 *	  c[i,j] = u (c[i,j], neighbours of c[i,j])
 *
 *	if not SOUTH ib = 1 else ib = 1+r
 *	if not NORTH ie = m else ie = m-r
 *	if not EAST jb = 1 else jb = 1+r
 *	if not WEST je = n else je = n-r
 *
 *	neighbours of c[i,j] are easily expressed:
 *		i == m ? s[1,j] : c[i+1,j],
 *		j == n ? e[i,1] : c[i,j+1],
 *		i == 1 ? n[m_n,j] : c[i-1,j],
 *		j == 1 ? w[i,n_w] : c[i,j-1]
 *
 * */
	if (debug)	
		fprintf(stdout, "[%d] (%d,%d) %dth step / current_ring:%d neighbour_ids:[%d;%d;%d;%d] \n", rank, x,y, loop, current_ring,ids[2],ids[3],ids[4],ids[5]);
	int nb_matrix_neigh = 0;
	for (int neigh = 2; neigh < 6; neigh++)
	{
		if (descr[neigh] == NULL)
			fprintf(stdout,"descr[%d] is NULL and it's ok\n", neigh);
		if (ids[neigh] == STARPU_MATRIX_INTERFACE_ID)
			nb_matrix_neigh += 1;
	}
	switch (nb_matrix_neigh) 
	{
		case 0: 
			ts.tv_nsec = 0*wait_ns_per_cell;
			break;
		case 1: 
		case 2: 
			ts.tv_nsec = (b-current_ring%b)*(b-current_ring%b)*wait_ns_per_cell;
			break;
		case 3: 
			ts.tv_nsec = (b-current_ring%b)*b*wait_ns_per_cell;
			break;
		case 4: 
			ts.tv_nsec = b*b*wait_ns_per_cell;
			break;
	}
//	starpu_usleep(ts.tv_nsec);
	struct timespec tr;
	int res = 1;
	while (res != 0)
	{
		res = nanosleep(&ts, &tr);
		ts = tr;
	}
		
//		float *xy   = (float *)STARPU_MATRIX_GET_PTR(descr[0]);
//		float *xm1y = (float *)STARPU_MATRIX_GET_PTR(descr[1]);
//		float *xp1y = (float *)STARPU_MATRIX_GET_PTR(descr[2]);
//		float *xym1 = (float *)STARPU_MATRIX_GET_PTR(descr[3]);
//		float *xyp1 = (float *)STARPU_MATRIX_GET_PTR(descr[4]);

//		*xy = (*xy + *xm1y + *xp1y + *xym1 + *xyp1) / 5;
//		fprintf(stdout, "VALUES: %2.2f %2.2f %2.2f %2.2f %2.2f\n", *xy, *xm1y, *xp1y, *xym1, *xyp1);
}

/* for many-cores architectures like Rome this is required because otherwise the touchfirst allocation happens on
 * stencil_cpu and this is simply bad for memory bandwidth */
struct starpu_codelet fill_cl =
{
	.cpu_funcs = {fill_cpu},
	.nbuffers = 1,
	.modes = { STARPU_W },
	.model = &starpu_perfmodel_nop,
};

struct starpu_codelet stencil_cl =
{
	.cpu_funcs = {stencil_cpu},
	.nbuffers = 6,
	.modes = { STARPU_W, STARPU_R, 
		   STARPU_R, STARPU_R, STARPU_R, STARPU_R},
	.model = &starpu_perfmodel_nop,
};

struct starpu_codelet stencil_repl_cl =
{
	.cpu_funcs = {stencil_cpu},
	.nbuffers = 6,
	.modes = { STARPU_W | STARPU_MPI_SAME, STARPU_R, 
		   STARPU_R, STARPU_R, STARPU_R, STARPU_R},
	.model = &starpu_perfmodel_nop,
};

#define CENTER 0
#define NORTH 1
#define SOUTH 2
#define EAST 3
#define WEST 4
#define NOWE 5
#define CENTER_BIS 5
#define SOWE 6
#define NOEA 7
#define SOEA 8

int _node_around(Cell*** cells, int x, int y, int s, int node)
{
	int xn, yn;
	int me;
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &me);
//	if (me == cells[0][x][y].owner) printf("[%d] testing (%d,%d) for -%d- | s=%d \n",me,x,y,node,s);
	for (xn = MAX(x - s, 0) ; xn < MIN(X, x + s + 1) ; xn ++) 
	{
		for (yn = MAX(y - s, 0) ; yn < MIN(Y, y + s + 1) ; yn ++) 
		{
//			if (me == cells[0][x][y].owner) printf("[%d] actual test (%d,%d)-(%d,%d) for -%d- | s=%d \n",me,x,y,xn,yn,node,s);
			if (cells[0][xn][yn].owner == node) return 1;
		} 
	} 
	return 0;
}

// TODO write a memoized version
int node_around(Cell*** cells, int x, int y, int s, int node)
{
	return _node_around(cells,x,y,s,node);
}

// TODO write a memoized version
int node_around_exact(Cell*** cells, int x, int y, int s, int node)
{
	int xn, yn;
//	if (x-s>=0) 
//	{
//		for (yn = MAX(y - s, 0) ; yn < MIN(Y, y + s + 1) ; yn ++) 
//		{
//			if (cells[0][x-s][yn].owner == node) return 1;
//		} 
//	} 
//	for (xn = MAX(x - s, 0) + 1 ; xn < MIN(X, x + s + 1) - 1 ; xn ++) 
//	{
//		if (y-s>=0   && cells[0][xn][y-s].owner == node) return 1;
//		if (y+s+1<Y && cells[0][xn][y+s+1].owner == node) return 1;
//	} 
//	if (x+s+1<X)
//	{
//		for (yn = MAX(y - s, 0) ; yn < MIN(Y, y + s + 1) ; yn ++) 
//		{
//			if (cells[0][x+s+1][yn].owner == node) return 1;
//		} 
//	} 
	int is_on_border = 0, distance;
	int distance_x, distance_y;
	int min_distance = 0; 
	min_distance = X + Y;
	for (xn = MAX(x - s, 0) ; xn < MIN(X, x + s + 1) ; xn ++) 
	{
		for (yn = MAX(y - s, 0) ; yn < MIN(Y, y + s + 1) ; yn ++) 
		{
			distance_x = abs(xn-x);
			distance_y = abs(yn-y);
			if (distance_x == s || distance_y == s)
			{
				if (cells[0][xn][yn].owner == node)
				{
					is_on_border = 1; // true
					min_distance = MIN(min_distance, distance_x + distance_y);
				}
			}
			else
			{
				if (cells[0][xn][yn].owner == node) return 0;
			}
		} 
	} 
	if (is_on_border)
		return min_distance == 2*s ? 2 : 1;
	return 0;
}

// TODO could be memoized as well
int* nodes_around(Cell*** cells, int x, int y, int s, int* nb_nodes)
{
	*nb_nodes = 0;
	int* nodes = NULL;
	int xn, yn, j;
	int already_there = 0;
	int rank;
	for (xn = MAX(x - s, 0) ; xn < MIN(X, x + s + 1) ; xn ++) 
	{
		for (yn = MAX(y - s, 0) ; yn < MIN(Y, y + s + 1) ; yn ++) 
		{
			rank = cells[0][xn][yn].owner;
			already_there = 0;
			for (j = 0; j < (*nb_nodes); j ++) 
			{
				if (nodes[j] == rank)
				{
					already_there = 1;	
				} 
			}
			if (!already_there)
			{
				(*nb_nodes)++;
				nodes = realloc(nodes, (*nb_nodes)*sizeof(int));
				nodes[(*nb_nodes)-1] = rank;
			}
		} 
	} 
	for (int node = 0; node < (*nb_nodes); node++) {
		if (nodes[node] == cells[0][x][y].owner) {
			int tmp_node = nodes[*(nb_nodes)-1];
			nodes[*(nb_nodes)-1] = cells[0][x][y].owner;
			nodes[node] = tmp_node;
			continue;
		}
	}	
	return nodes;
}

int is_frontier(int x, int y) {
	return x == 0 || x == X-1 || y == 0 || y == Y-1;
}

int my_distrib(int x, int y)
{
	/* 2D Block */
	int p_x = (X-1)/P + 1;
	int p_y = (Y-1)/Q + 1;
	return x / p_x + P*(y / p_y);
}


static void parse_args(int argc, char **argv)
{
	int i;
	for (i = 1; i < argc; i++)
	{
		if (strcmp(argv[i], "-iter") == 0)
		{
			char *argptr;
			niter = strtol(argv[++i], &argptr, 10);
		}
		else if (strcmp(argv[i], "-display") == 0)
			display = 1;
		else if (strcmp(argv[i], "-b") == 0)
		 	b = atoi(argv[++i]);
		else if (strcmp(argv[i], "-s") == 0)
		 	s = atoi(argv[++i]);
		else if (strcmp(argv[i], "-S") == 0)
			S = atoi(argv[++i]);
		else if (strcmp(argv[i], "-w") == 0)
			wait_ns_per_cell = atoi(argv[++i]);
		else if (strcmp(argv[i], "-X") == 0)
			X = atoi(argv[++i]);
		else if (strcmp(argv[i], "-Y") == 0)
			Y = atoi(argv[++i]);
		else if (strcmp(argv[i], "-P") == 0)
			P = atoi(argv[++i]);
		else if (strcmp(argv[i], "-Q") == 0)
			Q = atoi(argv[++i]);
		else if (strcmp(argv[i], "-replicating") == 0)
			replicated_variant = 1;
		else if (strcmp(argv[i], "-debug") == 0)
			debug = 1;
		else if (strcmp(argv[i], "-prof") == 0)
			prof = 1;
		else if (strcmp(argv[i], "-t") == 0)
			trial = atoi(argv[++i]);
	}
}

static void horizontal_partitioning(void *father_interface, void*child_interface, struct starpu_data_filter *f, unsigned id, unsigned nchunks) {
	struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface*) father_interface;
	struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface*) child_interface;
	if (nchunks != 3)
		fprintf(stderr,"Expecting 3 chunks...\n");	

	matrix_child->id = matrix_father->id;
	matrix_child->elemsize = matrix_father->elemsize;

	unsigned size = f->filter_arg;

	size_t offset;
	matrix_child->ny = matrix_father->ny;
	switch(id) 
	{
		case 0:
			offset = 0;
			matrix_child->nx = MAX(size,1);
			break;
		case 2:
			offset = matrix_father->ld * MAX(matrix_father->nx - size, 1);
			matrix_child->nx = MAX(size,1);
			break;
		case 1:
			offset = matrix_father->ld * MAX(size, 1);
			matrix_child->nx = MAX(matrix_father->nx - 2*size,1);
			break;
	}	
	offset *= matrix_child->elemsize;
	if (matrix_father->dev_handle)
	{
		if (matrix_father->ptr)
			matrix_child->ptr = matrix_father->ptr + offset;
		matrix_child->ld = matrix_father->ld;
		matrix_child->dev_handle = matrix_father->dev_handle;
		matrix_child->offset = matrix_father->offset + offset;
		matrix_child->allocsize = matrix_child->ld * matrix_child->ny * matrix_child->elemsize;
	}
	else
	{
		matrix_child->allocsize = matrix_child->nx * matrix_child->ny * matrix_child->elemsize;
	}
}

struct starpu_data_filter f_horz = {
	.filter_func = horizontal_partitioning,
	.nchildren = 3,
	.filter_arg = 1 // default value, (not-asserted) should be lower than block size
};

static void vertical_partitioning(void *father_interface, void*child_interface, struct starpu_data_filter *f, unsigned id, unsigned nchunks) {
	struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface*) father_interface;
	struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface*) child_interface;
	if (nchunks != 3)
		fprintf(stderr,"Expecting 3 chunks...\n");	

	matrix_child->id = matrix_father->id;
	matrix_child->elemsize = matrix_father->elemsize;

	unsigned size = f->filter_arg;

	size_t offset;
	matrix_child->nx = matrix_father->nx;
	switch(id) 
	{
		case 0:
			offset = 0;
			matrix_child->ny = MAX(size,1);
			break;
		case 2:
			offset = matrix_father->ld - MAX(size,1);
			matrix_child->ny = MAX(size,1);
			break;
		case 1:
			offset = MAX(size,1);
			matrix_child->ny = MAX(matrix_father->ny - 2*size,1);
			break;
	}	
	offset *= matrix_child->elemsize;
	if (matrix_father->dev_handle)
	{
		if (matrix_father->ptr)
			matrix_child->ptr = matrix_father->ptr + offset;
		matrix_child->ld = matrix_father->ld;
		matrix_child->dev_handle = matrix_father->dev_handle;
		matrix_child->offset = matrix_father->offset + offset;
		matrix_child->allocsize = matrix_child->ld * matrix_child->ny * matrix_child->elemsize;
	}
	else
	{
		matrix_child->allocsize = matrix_child->nx * matrix_child->ny * matrix_child->elemsize;
	}
}

struct starpu_data_filter f_vert = {
	.filter_func = vertical_partitioning,
	.nchildren = 3,
	.filter_arg = 1 // default value, (not-asserted) should be lower than block size
};

void submit_classical_variant(Cell*** cells, starpu_data_handle_t*** data_handles, starpu_data_handle_t**** children_handles) 
{
	int me, size, dummy = 0;
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &me);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &size);
	int nb_handles = 6;		
	struct starpu_data_descr *descrs = NULL;
	enum starpu_data_interface_id* ids = NULL;
	int loop, x, y, i;

	int north_fr, south_fr, east_fr, west_fr;

	int present[size];
	int nb_present = 0;
	int ff;

	descrs = malloc(nb_handles * sizeof(struct starpu_data_descr));
	descrs[0].mode = STARPU_W; 
	descrs[1].mode = STARPU_R;
	descrs[2].mode = STARPU_R;
	descrs[3].mode = STARPU_R;
	descrs[4].mode = STARPU_R;
	descrs[5].mode = STARPU_R;
	ids = calloc(nb_handles,sizeof(enum starpu_data_interface_id));
	for(loop=0 ; loop<niter; loop++)
	{
		for (x = 1; x < X-1; x++)
		{
			for (y = 1; y < Y-1; y++)
			{
				for (i = 0; i < size; i++)
				{
					present[i] = 0;
				}
				nb_present = 0;
				/* read/write so every stencil use that */
				north_fr = y+1 == Y-1?0:loop%S;
				south_fr = y-1 == 0?0:loop%S;
				west_fr = x-1 == 0?0:loop%S;
				east_fr = x+1 == X-1?0:loop%S;

				descrs[0].handle = data_handles[(loop+1)%S][x][y];
				descrs[1].handle = data_handles[loop%S][x][y];
				descrs[2].handle = is_frontier(x,y+1) || cells[0][x][y+1].owner == cells[0][x][y].owner ? data_handles[north_fr][x][y+1] 
															: children_handles[north_fr][x][y+1][SOUTH];
				descrs[3].handle = is_frontier(x,y-1) || cells[0][x][y-1].owner == cells[0][x][y].owner ? data_handles[south_fr][x][y-1] 
															: children_handles[south_fr][x][y-1][NORTH];
				descrs[4].handle = is_frontier(x+1,y) || cells[0][x+1][y].owner == cells[0][x][y].owner ? data_handles[east_fr][x+1][y] 
										    	  				: children_handles[east_fr][x+1][y][WEST];
				descrs[5].handle = is_frontier(x-1,y) || cells[0][x-1][y].owner == cells[0][x][y].owner ? data_handles[west_fr][x-1][y] 
															: children_handles[west_fr][x-1][y][EAST];

				if (debug) printf("[%d] STUFF (%d,%d) %p%p:%p%p%p%p\n", me, x,y,
					descrs[0].handle,
					descrs[1].handle,
					descrs[2].handle,
					descrs[3].handle,
					descrs[4].handle,
					descrs[5].handle
					);
				ids[0] = starpu_data_get_interface_id(descrs[0].handle);
				ids[1] = starpu_data_get_interface_id(descrs[1].handle);
				ids[2] = starpu_data_get_interface_id(descrs[2].handle);
				ids[3] = starpu_data_get_interface_id(descrs[3].handle);
				ids[4] = starpu_data_get_interface_id(descrs[4].handle);
				ids[5] = starpu_data_get_interface_id(descrs[5].handle);

				present[ cells[0][x][y].owner ] = 1;
				present[ cells[0][x+1][y].owner ] = 1;
				present[ cells[0][x-1][y].owner ] = 1;
				present[ cells[0][x][y+1].owner ] = 1;
				present[ cells[0][x][y-1].owner ] = 1;
				for (i = 0; i < size; i++)
				{
					nb_present += present[i];
				}
	
				if (me == cells[0][x][y].owner || 
				    me == cells[0][x+1][y].owner || 
				    me == cells[0][x-1][y].owner || 
				    me == cells[0][x][y-1].owner || 
				    me == cells[0][x][y+1].owner)
					starpu_mpi_task_insert(MPI_COMM_WORLD, &stencil_cl, 
								STARPU_VALUE, &dummy, sizeof(dummy),
								STARPU_VALUE, &loop, sizeof(loop),
								STARPU_VALUE, &x, sizeof(x),
								STARPU_VALUE, &y, sizeof(y),
								STARPU_VALUE, &wait_ns_per_cell, sizeof(wait_ns_per_cell),
								STARPU_VALUE, ids, 6*sizeof(ids[0]),
								STARPU_PRIORITY, nb_present,
								STARPU_DATA_MODE_ARRAY, descrs, nb_handles,
							       0);
			}
		}
		for (ff = 0; ff < S; ff++)
		{
			for (x = 1; x < X-1; x++)
			{
				for (y = 1; y < Y-1; y++)
				{
					starpu_mpi_cache_flush(MPI_COMM_WORLD, data_handles[ff][x][y]);
					starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][NORTH]);
					starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][SOUTH]);
					starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][EAST]);
					starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][WEST]);
				}
			}
		}
	}
	free(ids);
	free(descrs);
}


static void cardinal_partitioning(void *father_interface, void*child_interface, struct starpu_data_filter *f, unsigned id, unsigned nchunks) {
	struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface*) father_interface;
	struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface*) child_interface;
	if (nchunks != 4)
		fprintf(stderr,"Expecting 4 chunks...\n");	

	matrix_child->id = matrix_father->id;
	matrix_child->elemsize = matrix_father->elemsize;

	unsigned size = MIN(f->filter_arg, MIN(matrix_father->nx, matrix_father->ny));

	matrix_child->nx = MAX(size,1);
	matrix_child->ny = MAX(size,1);
	size_t offset;
	switch(id) 
	{
		case 0: // NO WE
			offset = 0;
			break;
		case 1: // NO EA
			offset = matrix_father->ld * MAX(matrix_father->nx - size, 1);
			break;
		case 2: // SO WE
			offset = matrix_father->ld - MAX(size,1);
			break;
		case 3: // SO EA
			offset = matrix_father->ld - MAX(size,1);
			offset += matrix_father->ld * MAX(matrix_father->nx - size, 1);
			break;
	}	
	offset *= matrix_child->elemsize;
	if (matrix_father->dev_handle)
	{
		if (matrix_father->ptr)
			matrix_child->ptr = matrix_father->ptr + offset;
		matrix_child->ld = matrix_father->ld;
		matrix_child->dev_handle = matrix_father->dev_handle;
		matrix_child->offset = matrix_father->offset + offset;
		matrix_child->allocsize = matrix_child->ld * matrix_child->ny * matrix_child->elemsize;
	}
	else
	{
		matrix_child->allocsize = matrix_child->nx * matrix_child->ny * matrix_child->elemsize;
	}
}

struct starpu_data_filter f_card = {
	.filter_func = cardinal_partitioning, 
	.nchildren = 4,
	.filter_arg = 1 // default value, (not-asserted) should be lower than block size
};

static void horizontal_repl_partitioning(void *father_interface, void*child_interface, struct starpu_data_filter *f, unsigned id, unsigned nchunks) {
	struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface*) father_interface;
	struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface*) child_interface;
	if (nchunks != 2)
		fprintf(stderr,"Expecting 2 chunks...\n");	

	matrix_child->id = matrix_father->id;
	matrix_child->elemsize = matrix_father->elemsize;

	unsigned size = MIN(f->filter_arg, matrix_father->nx);

	size_t offset;
	matrix_child->nx = matrix_father->nx;
	matrix_child->ny = MAX(size,1);
	switch(id) 
	{
		case 0:
			offset = 0;
			break;
		case 1:
			offset = matrix_father->ld - MAX(size,1); // + 1;
			break;
	}	
	offset *= matrix_child->elemsize;
	if (matrix_father->dev_handle)
	{
		if (matrix_father->ptr)
			matrix_child->ptr = matrix_father->ptr + offset;
		matrix_child->ld = matrix_father->ld;
		matrix_child->dev_handle = matrix_father->dev_handle;
		matrix_child->offset = matrix_father->offset + offset;
		matrix_child->allocsize = matrix_child->ld * matrix_child->ny * matrix_child->elemsize;
	}
	else
	{
		matrix_child->allocsize = matrix_child->nx * matrix_child->ny * matrix_child->elemsize;
	}
}

struct starpu_data_filter f_horz_repl = {
	.filter_func = horizontal_repl_partitioning,
	.nchildren = 2,
	.filter_arg = 1 // default value, (not-asserted) should be lower than block size
};

static void vertical_repl_partitioning(void *father_interface, void*child_interface, struct starpu_data_filter *f, unsigned id, unsigned nchunks) {
	struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface*) father_interface;
	struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface*) child_interface;
	if (nchunks != 2)
		fprintf(stderr,"Expecting 2 chunks...\n");	

	matrix_child->id = matrix_father->id;
	matrix_child->elemsize = matrix_father->elemsize;

	unsigned size = MIN(f->filter_arg, matrix_father->ny);

	size_t offset;
	matrix_child->nx = MAX(size,1);
	matrix_child->ny = matrix_father->ny;
	switch(id) 
	{
		case 0:
			offset = 0;
			break;
		case 1:
			offset = matrix_father->ld * MAX(matrix_father->nx - size, 1);
			break;
	}	
	offset *= matrix_child->elemsize;
	if (matrix_father->dev_handle)
	{
		if (matrix_father->ptr)
			matrix_child->ptr = matrix_father->ptr + offset;
		matrix_child->ld = matrix_father->ld;
		matrix_child->dev_handle = matrix_father->dev_handle;
		matrix_child->offset = matrix_father->offset + offset;
		matrix_child->allocsize = matrix_child->ld * matrix_child->ny * matrix_child->elemsize;
	}
	else
	{
		matrix_child->allocsize = matrix_child->nx * matrix_child->ny * matrix_child->elemsize;
	}
}

struct starpu_data_filter f_vert_repl = {
	.filter_func = vertical_repl_partitioning,
	.nchildren = 2,
	.filter_arg = 1 // default value, (not-asserted) should be lower than block size
};

starpu_data_handle_t replication_get_correct_handle(int node, int loop, int ring_size, int x, int xd, int y, int yd, int orientation, int od, starpu_data_handle_t**** children_handles, Cell*** cells) {
 	int border = x == 0 || x == X-1 || y == 0 || y == Y-1;
	starpu_data_handle_t handle = children_handles[border ? 0 : loop % S][x][y][orientation];
        if (xd >= 0 && xd < X && yd >= 0 && yd < Y &&
		!node_around(cells, xd, yd, ring_size, node))
	{
		handle = children_handles[border ? 0 : loop % S][x][y][od];
	}
	return handle;
}

void submit_replicated_variant(Cell*** cells, starpu_data_handle_t dummy_handle, starpu_data_handle_t*** data_handles, starpu_data_handle_t**** children_handles)
{
	int me, size;
	int nb_nodes;
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &me);
	int nb_handles = 6;		
	int current_ring, ring_size;
	int read_ring, write_ring;
	int read_size, write_size;
	int *nodes = NULL;
	struct starpu_data_descr *descrs = NULL;
	enum starpu_data_interface_id* ids = NULL;
	int loop, x, y, j, i, ff;
	int bord_northern;
	int bord_southern;
	int bord_western;
	int bord_eastern;
	int northern;
	int southern;
	int western;
	int eastern;
	int north_fr;
	int south_fr;
	int west_fr;
	int east_fr;
	int in_nodes;

	descrs = malloc(nb_handles * sizeof(struct starpu_data_descr));
	descrs[0].mode = STARPU_W | STARPU_MPI_SAME; 
	descrs[1].mode = STARPU_R;
	descrs[2].mode = STARPU_R;
	descrs[3].mode = STARPU_R;
	descrs[4].mode = STARPU_R;
	descrs[5].mode = STARPU_R;
	ids = calloc(nb_handles,sizeof(enum starpu_data_interface_id));

	write_ring = s-1; // max value
	read_ring = write_ring + 1;
	read_size = (read_ring - 1)/ b + 1;
	int max_ring_size = read_size;

	for(loop=0 ; loop<niter; loop++)
	{
//		current_ring = s - loop % (s+1);
//		ring_size = current_ring == 0 ? 0 : (current_ring - 1)/ b + 1;
		// weird stuff ... write_ring = (loop-1+s)%s; //# + 1;
		write_ring = s - loop%s - 1; //# + 1;
		current_ring = b - write_ring;
		// wbbetter : write_ring = s - loop%s; //# + 1;
		read_ring = write_ring + 1;
		write_size = write_ring == 0 ? 0 : (write_ring - 1)/ b + 1;
		read_size = (read_ring - 1)/ b + 1;
		for (x = 1; x < X-1; x++)
		{
			for (y = 1; y < Y-1; y++)
			{
				/* slightly better abstracton to do this whole thing */
				nodes = nodes_around(cells, x, y, read_size, &nb_nodes);
				/* read/write so every stencil use that */

				in_nodes = 0;
				for (j = 0; j < nb_nodes; j ++)
				{
					if (nodes[j] == me)
					{
						in_nodes = 1;
						continue;
					}
				}
//				for(int j = 0; j < nb_nodes; j++) {
//					fprintf(stdout,"[%d] %d (%d,%d|%d) nodes[%d] : %d (over %d nodes)\n", me, loop, x,y,cells[0][x][y].owner,  j, nodes[j], nb_nodes);
//				}
				if (in_nodes)
				{
					for (j = 0; j < nb_nodes; j ++)
					{
						/* stencil dependent, IDK how to abstract more */
						/* i think it could work for grid that are unstructured as long as cells have 
 						 * some kind of adjacency that can be exploited ... */
						// Anyway, at this point, as long as any domain expert can write their
						// equation in stencil_cpu (or stencil_gpu) we are happy



						// FIXME Something below, including and before insert, segfaults : logic is
						// obviously failed
						// At least I can fix it now :|

// Seems like I did something, at least ; just ... It will be long to explain and detail :|



						// /!\ je parle pas parce que je suis trop frustré
						// donc
						// en ros, là, les fonctions en dessous, elles disent "est-ce que la
						// case est + au nord / sud / ... " de la case x,y
						// sauf que tu vois bien que je mets pas "x,y" en arguments, je mets ...
						// ?
						// je mets quoi ?
						// pas ça, exactement, je mets x(+/-)1, y(+/-)1
						// et du coup, ça fait ce que je veux ; mais j'avais pas pensé à un truc
						northern = node_around(cells,x,y+1,max_ring_size,nodes[j]);
						southern = node_around(cells,x,y-1,max_ring_size,nodes[j]);
						eastern  = node_around(cells,x+1,y,max_ring_size,nodes[j]);
						western  = node_around(cells,x-1,y,max_ring_size,nodes[j]);
						int adj = northern + southern + western + eastern;
						if (debug && me == nodes[j])
							fprintf(stdout,"[%d] (%d,%d) %d sz R:%d W:%d\n",me,x,y,adj,read_size,write_size);
						descrs[0].handle = NULL;
						descrs[1].handle = NULL;
						if (adj == 4) {
							descrs[0].handle = data_handles[(loop+1)%S][x][y];
							descrs[1].handle = data_handles[loop%S][x][y];
						} else if (adj == 3) {
							if (!southern) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][NORTH];
								descrs[1].handle = children_handles[loop%S    ][x][y][NORTH];
							} else if (!northern) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][SOUTH];
								descrs[1].handle = children_handles[loop%S    ][x][y][SOUTH];
							} else if (!western) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][EAST];
								descrs[1].handle = children_handles[loop%S    ][x][y][EAST];
							} else if (!eastern) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][WEST];
								descrs[1].handle = children_handles[loop%S    ][x][y][WEST];
							}
						} else {
							if (southern && eastern) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][SOEA];
								descrs[1].handle = children_handles[loop%S    ][x][y][SOEA];
							} else if (southern && western) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][SOWE];
								descrs[1].handle = children_handles[loop%S    ][x][y][SOWE];
							} else if (northern && eastern) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][NOEA];
								descrs[1].handle = children_handles[loop%S    ][x][y][NOEA];
							} else if (northern && western) {
								descrs[0].handle = children_handles[(loop+1)%S][x][y][NOWE];
								descrs[1].handle = children_handles[loop%S    ][x][y][NOWE];
							}
						}
						bord_northern = node_around_exact(cells,x,y+1,max_ring_size,nodes[j]);
						bord_southern = node_around_exact(cells,x,y-1,max_ring_size,nodes[j]);
						bord_eastern  = node_around_exact(cells,x+1,y,max_ring_size,nodes[j]);
						bord_western  = node_around_exact(cells,x-1,y,max_ring_size,nodes[j]);
						north_fr = y+1 == Y-1?0:loop%S;
						south_fr = y-1 == 0?0:loop%S;
						west_fr = x-1 == 0?0:loop%S;
						east_fr = x+1 == X-1?0:loop%S;

						// NEW WAY TO DO IT ... TERNARY OPERATOR DO NOT WORK :(

						if (!northern) {
							descrs[2].handle = dummy_handle;
						} else {
							if (!bord_northern) {
								descrs[2].handle = data_handles[north_fr][x][y+1];
							} else {
								if (adj == 4) {
									descrs[2].handle = children_handles[north_fr][x][y+1][SOUTH];
								} else {
									if (bord_northern == 1 || is_frontier(x,y+1)) {
										descrs[2].handle = western ? children_handles[north_fr][x][y+1][WEST]
													   : children_handles[north_fr][x][y+1][EAST];
									} else {
										descrs[2].handle = western ? children_handles[north_fr][x][y+1][SOWE]
													   : children_handles[north_fr][x][y+1][SOEA];
									}
								}
							}
						}
						if (!southern) {
							descrs[3].handle = dummy_handle;
						} else {
							if (!bord_southern) {
								descrs[3].handle = data_handles[south_fr][x][y-1];
							} else {
								if (adj == 4) {
									descrs[3].handle = children_handles[south_fr][x][y-1][NORTH];
								} else {
									if (bord_southern == 1 || is_frontier(x,y-1)) {
										descrs[3].handle = western ? children_handles[south_fr][x][y-1][WEST]
												   	   : children_handles[south_fr][x][y-1][EAST];
									} else {
										descrs[3].handle = western ? children_handles[south_fr][x][y-1][NOWE]
													   : children_handles[south_fr][x][y-1][NOEA];
									}
								}
							}
						}
						if (!eastern) {
							descrs[4].handle = dummy_handle;
						} else {
							if (!bord_eastern) {
								descrs[4].handle = data_handles[east_fr][x+1][y];
							} else {
								if (adj == 4) {
									descrs[4].handle = children_handles[east_fr][x+1][y][WEST];
								} else {
									if (bord_eastern == 1 || is_frontier(x+1,y)) {
										descrs[4].handle = northern ? children_handles[east_fr][x+1][y][NORTH]
													    : children_handles[east_fr][x+1][y][SOUTH];
									} else {
										descrs[4].handle = northern ? children_handles[east_fr][x+1][y][NOWE]
													    : children_handles[east_fr][x+1][y][SOWE];
									}
								}
							}
						}
						if (!western) {
							descrs[5].handle = dummy_handle;
						} else {
							if (!bord_western) {
								descrs[5].handle = data_handles[west_fr][x-1][y];
							} else {
								if (adj == 4) {
									descrs[5].handle = children_handles[west_fr][x-1][y][EAST];
								} else {
									if (bord_western == 1 || is_frontier(x-1,y)) {
										descrs[5].handle = northern ? children_handles[west_fr][x-1][y][NORTH]
												   	    : children_handles[west_fr][x-1][y][SOUTH];
									} else {
										descrs[5].handle = northern ? children_handles[west_fr][x-1][y][NOEA]
													    : children_handles[west_fr][x-1][y][SOEA];
									}
								}
							}
						}

						if (debug)
						{
							fprintf(stdout,"[%d] (%d,%d) - %d | %d%d%d%d %d%d%d%d |%d %d ; %d %d %d %d\n", me, x,y,loop,
									northern, southern, eastern, western,
									bord_northern, bord_southern, bord_eastern, bord_western,
									starpu_mpi_data_get_tag(descrs[0].handle),
									starpu_mpi_data_get_tag(descrs[1].handle),
									starpu_mpi_data_get_tag(descrs[2].handle),
									starpu_mpi_data_get_tag(descrs[3].handle),
									starpu_mpi_data_get_tag(descrs[4].handle),
									starpu_mpi_data_get_tag(descrs[5].handle));
						}
						ids[0] = starpu_data_get_interface_id(descrs[0].handle);
						ids[1] = starpu_data_get_interface_id(descrs[1].handle);
						ids[2] = starpu_data_get_interface_id(descrs[2].handle);
						ids[3] = starpu_data_get_interface_id(descrs[3].handle);
						ids[4] = starpu_data_get_interface_id(descrs[4].handle);
						ids[5] = starpu_data_get_interface_id(descrs[5].handle);
						starpu_mpi_task_insert(MPI_COMM_WORLD, &stencil_repl_cl, 
									STARPU_VALUE, &current_ring, sizeof(current_ring),
									STARPU_VALUE, &loop, sizeof(loop),
									STARPU_VALUE, &x, sizeof(x),
									STARPU_VALUE, &y, sizeof(y),
									STARPU_VALUE, &wait_ns_per_cell, sizeof(wait_ns_per_cell),
									STARPU_VALUE, ids, 6*sizeof(ids[0]),
									STARPU_PRIORITY, nb_nodes,
									STARPU_DATA_MODE_ARRAY, descrs, nb_handles,
									STARPU_EXECUTE_ON_NODE, nodes[j],
								       0);
					}
				}
			}
		}
		if (read_ring == 1) {
			if (debug)
				fprintf(stdout, "[%d] FLUSHING (%d) (%d:%d)x(%d:%d)!\n", me, loop,1,X-2,1,Y-2);
			for (ff = 0; ff < S; ff++)
			{
//				we should not be flushing frontiers that do not change
				for (x = 1; x < X-1; x++)
				{
					for (y = 1; y < Y-1; y++)
					{
						starpu_mpi_cache_flush(MPI_COMM_WORLD, data_handles[ff][x][y]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][NORTH]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][SOUTH]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][EAST]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][WEST]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][SOWE]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][SOEA]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][NOWE]);
						starpu_mpi_cache_flush(MPI_COMM_WORLD, children_handles[ff][x][y][NOEA]);
					}
				}
			}
		}
	}
	free(descrs);
	free(ids);
}

int main(int argc, char **argv)
{
	int size, me, loop, x, y;
	starpu_data_handle_t dummy_handle;
	char dummy_variable;
	int ret;

	ret = starpu_mpi_init_conf(&argc, &argv, 1, MPI_COMM_WORLD, NULL);
	STARPU_CHECK_RETURN_VALUE(ret, "starpu_mpi_init_conf");
	starpu_mpi_comm_rank(MPI_COMM_WORLD, &me);
	starpu_mpi_comm_size(MPI_COMM_WORLD, &size);

	if (starpu_cpu_worker_get_count() < 1)
	{
		starpu_mpi_shutdown();
		if (me == 0) {
			fprintf(stderr, "We need at least 1 CPU worker.\n");
			return 77;
		}
		return 0;
	}

	if (debug)
		fprintf(stdout,"[%d] Parsing arguments\n", me);
	parse_args(argc, argv);
	if (replicated_variant) {
		if (s / b >= X/Q-1 && s % b > 0) {
			starpu_mpi_shutdown();
			if (me == 0) {
				fprintf(stderr, "not enough blocks\n");
				return 78;
			}
			return 0;
		}
	}
	if (me == 0)
	{
		fprintf(stdout,"World is of size %d\n", size);
		fprintf(stdout,"=== PROBLEM ===\n");
		fprintf(stdout," X x Y = %d x %d cells over %d steps\n",X*b, Y*b, niter);
		fprintf(stdout," ( X x Y = %d x %d handles )\n",X, Y);
		fprintf(stdout," %d iterations\n", niter);
		fprintf(stdout," %d saves grids\n", S);
		fprintf(stdout," handle domain size: %d x %d\n", b,b);
		if (replicated_variant)
			fprintf(stdout," --> replication ring: %d\n", s);
		else
			fprintf(stdout," --> classical variant\n");
		fprintf(stdout," cell buffer time = %d ns\n", wait_ns_per_cell);
		fprintf(stdout,"===============\n");
		fprintf(stdout,"matrix_id : %d\n", STARPU_MATRIX_INTERFACE_ID);
		fprintf(stdout,"variable_id : %d\n", STARPU_VARIABLE_INTERFACE_ID);
	}
	starpu_mpi_barrier(MPI_COMM_WORLD);

	Cell*** cells = NULL;
	starpu_data_handle_t*** data_handles = NULL;
	starpu_data_handle_t**** children_handles = NULL;
	cells = calloc(S,sizeof(Cell**));
	for (int grid = 0 ; grid < S; grid++) {
		cells[grid] = calloc(X,sizeof(Cell*));
		for (x = 0 ; x < X; x++) {
			cells[grid][x] = calloc(Y, sizeof(Cell));
		}
	}
	data_handles= calloc(S,sizeof(starpu_data_handle_t**));
	children_handles= calloc(S,sizeof(starpu_data_handle_t***));
	for (int grid = 0 ; grid < S; grid++) {
		data_handles[grid] = calloc(X, sizeof(starpu_data_handle_t*));
		children_handles[grid] = calloc(X, sizeof(starpu_data_handle_t**));
		for (x = 0 ; x < X; x++) {
			data_handles[grid][x] = calloc(Y, sizeof(starpu_data_handle_t));
			children_handles[grid][x] = calloc(Y, sizeof(starpu_data_handle_t*));
			for (y = 0 ; y < Y; y++) {
				if (replicated_variant)
				{
					children_handles[grid][x][y] = calloc(9, sizeof(starpu_data_handle_t));
				}
				else
				{
					children_handles[grid][x][y] = calloc(6, sizeof(starpu_data_handle_t));
				}
			}
		}
	}

	starpu_mpi_barrier(MPI_COMM_WORLD);

	/* Initial data values */
	long tag = 0;
	starpu_srand48((long int)clock());
	starpu_variable_data_register(&dummy_handle, 0, (uintptr_t)&(dummy_variable), sizeof(char));
	starpu_mpi_data_register(dummy_handle, tag++, STARPU_MPI_PER_NODE);
	starpu_data_handle_t vert_handles[3];
	starpu_data_handle_t horz_handles[3];
	starpu_data_handle_t vert_repl_handles[2];
	starpu_data_handle_t horz_repl_handles[2];
	starpu_data_handle_t card_handles[4];
	f_vert_repl.filter_arg = 1 + (s-1) % b;
	f_horz_repl.filter_arg = 1 + (s-1) % b;
	f_card.filter_arg = 1+ (s - 1) % b;
	int ssz = 1 + (s-1)%b;
	if (debug)
		fprintf(stdout, "FILTER_ARG %d\n", f_card.filter_arg);
	for(x = 0; x < X; x++)
	{
		for (y = 0; y < Y; y++)
		{
			int mpi_rank = my_distrib(x, y);
			// ok this feels weird but let's say it's ok
			int ny, nx;
			nx = x == 0 || x == X-1 ? 1 : b;
			ny = y == 0 || y == Y-1 ? 1 : b;
			if (mpi_rank == me) {
//				fprintf(stderr, "[%d] Owning data[%d][%d]\n", me, x, y);
			}
			for(int grid=0; grid < S; grid++)
			{
				if (mpi_rank == me) {
					cells[grid][x][y].center = malloc(nx*ny*sizeof(float));
					// /!\ do not do that to avoid touchfirst allocation on the main thread
					//
					//for (int xx = 0; xx < nx; xx++) {
					//	for (int yy = 0; yy < ny; yy++) {
					//		cells[grid][x][y].center[xx*ny + yy] = xx*ny + yy;
					//	}
					//}
					// WTF WAS THIS CALL ???? starpu_matrix_data_register(&data_handles[grid][x][y], 0, (uintptr_t)&(cells[grid][x][y].center), ny, ny,nx,sizeof(float));
					starpu_matrix_data_register(&data_handles[grid][x][y], 0, (uintptr_t) cells[grid][x][y].center, nx, nx,ny,sizeof(float));
					if (nx == b && ny == b)
						starpu_task_insert(&fill_cl,STARPU_W, data_handles[grid][x][y],0);
					if (!replicated_variant)
					{
						if (ny > 1)
						{
							starpu_data_partition_plan(data_handles[grid][x][y], &f_vert, vert_handles);
							children_handles[grid][x][y][NORTH] = vert_handles[2];
							children_handles[grid][x][y][CENTER_BIS] = vert_handles[1];
							children_handles[grid][x][y][SOUTH] = vert_handles[0]; 
						}
						if (nx > 1)
						{
							starpu_data_partition_plan(data_handles[grid][x][y], &f_horz, horz_handles);
							children_handles[grid][x][y][EAST]  = horz_handles[0];
							children_handles[grid][x][y][CENTER] = horz_handles[1];
							children_handles[grid][x][y][WEST]  = horz_handles[2];
						}
					}
					else
					{
						if (ny > 1)
						{
							starpu_data_partition_plan(data_handles[grid][x][y], &f_horz_repl, horz_handles);
							children_handles[grid][x][y][NORTH] = horz_handles[1];
							children_handles[grid][x][y][SOUTH] = horz_handles[0];
						}
						if (nx > 1)
						{
							starpu_data_partition_plan(data_handles[grid][x][y], &f_vert_repl, vert_handles);
							children_handles[grid][x][y][EAST] = vert_handles[1];
							children_handles[grid][x][y][WEST] = vert_handles[0];
						}
						if (!is_frontier(x,y))
						{
							// arbitrary: we use NS then EW | impossible to partition twice
							//starpu_data_partition_plan(children_handles[grid][x][y][NORTH], &f_horz_repl, horz_handles);
							//children_handles[grid][x][y][NOWE] = horz_handles[0];
							//children_handles[grid][x][y][NOEA] = horz_handles[1];
							//starpu_data_partition_plan(children_handles[grid][x][y][SOUTH], &f_horz_repl, horz_handles);
							//children_handles[grid][x][y][SOWE] = horz_handles[0];
							//children_handles[grid][x][y][SOEA] = horz_handles[1];
							starpu_data_partition_plan(data_handles[grid][x][y], &f_card, card_handles);
							children_handles[grid][x][y][SOEA] = card_handles[0];
							children_handles[grid][x][y][SOWE] = card_handles[1];
							children_handles[grid][x][y][NOEA] = card_handles[2];
							children_handles[grid][x][y][NOWE] = card_handles[3];
						}
					}
				}
				else
				{
					starpu_matrix_data_register(&data_handles[grid][x][y], -1,(uintptr_t)NULL, nx, nx,ny,sizeof(float));
					if (!replicated_variant)
					{
						if (ny > 1)
						{
							starpu_matrix_data_register(&children_handles[grid][x][y][NORTH], -1,(uintptr_t)NULL, nx, nx, 1,sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][SOUTH], -1,(uintptr_t)NULL, nx, nx, 1,sizeof(float));
						}
						if (nx > 1)
						{
							starpu_matrix_data_register(&children_handles[grid][x][y][WEST],  -1,(uintptr_t)NULL, 1, 1, ny, sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][EAST],  -1,(uintptr_t)NULL, 1, 1, ny, sizeof(float));
						}
					}
					else
					{
						if (ny > 1)
						{
							starpu_matrix_data_register(&children_handles[grid][x][y][NORTH],-1,(uintptr_t)NULL, nx,nx,ssz, sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][SOUTH],-1,(uintptr_t)NULL, nx,nx,ssz, sizeof(float));
						}
						if (nx > 1)
						{
							starpu_matrix_data_register(&children_handles[grid][x][y][WEST], -1,(uintptr_t)NULL, ssz,ssz,ny,sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][EAST], -1,(uintptr_t)NULL, ssz,ssz,ny,sizeof(float));
						}
						if (!is_frontier(x,y))
						{
							starpu_matrix_data_register(&children_handles[grid][x][y][NOEA], -1,(uintptr_t)NULL, ssz, ssz, ssz, sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][SOEA], -1,(uintptr_t)NULL, ssz, ssz, ssz, sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][NOWE], -1,(uintptr_t)NULL, ssz, ssz, ssz, sizeof(float));
							starpu_matrix_data_register(&children_handles[grid][x][y][SOWE], -1,(uintptr_t)NULL, ssz, ssz, ssz, sizeof(float));
						}
					}
				}
				starpu_mpi_data_register(data_handles[grid][x][y], tag++, mpi_rank);
				if (debug)
					printf("TAG C [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
				if (!replicated_variant)
				{
					if (ny > 1)
					{
						starpu_mpi_data_register(children_handles[grid][x][y][NORTH], tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][SOUTH], tag++, mpi_rank);
					}
					if (nx > 1)
					{
						starpu_mpi_data_register(children_handles[grid][x][y][WEST], tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][EAST], tag++, mpi_rank);
					}
				}
				else
				{
					if (ny > 1)
					{
						starpu_mpi_data_register(children_handles[grid][x][y][NORTH], tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][SOUTH], tag++, mpi_rank);
						if (debug) {
							printf("TAG N [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
							printf("TAG S [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
						}
					}
					if (nx > 1)
					{
						starpu_mpi_data_register(children_handles[grid][x][y][WEST],  tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][EAST],  tag++, mpi_rank);
						if (debug)
						{
							printf("TAG W [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
							printf("TAG E [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
						}
					}
					if (!is_frontier(x,y))
					{
						starpu_mpi_data_register(children_handles[grid][x][y][NOEA],  tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][SOEA],  tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][NOWE],  tag++, mpi_rank);
						starpu_mpi_data_register(children_handles[grid][x][y][SOWE],  tag++, mpi_rank);
						if (debug) 
						{
							printf("TAG NE [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
							printf("TAG SE [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
							printf("TAG NW [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
							printf("TAG SW [%d]-(%d,%d) = %d\n",grid,x,y,tag-1);
						}
					}
				}
				cells[grid][x][y].owner = mpi_rank;
			}
		}
	}
	starpu_mpi_barrier(MPI_COMM_WORLD);
	if (debug)
	{

		int lx = 2;
		int ly = 1;
		struct starpu_matrix_interface *matrixi;
		float* matrix;
		int acq;
		printf("let's look at the partitioning (%d,%d owned by %d)\n", lx, ly, cells[0][lx][ly].owner);
		printf("original data\n");
		matrixi = starpu_data_get_interface_on_node(data_handles[0][lx][ly], STARPU_MAIN_RAM);
		printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d ; offset %d\n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize, matrixi->offset);
		if (me == cells[0][lx][ly].owner) {
			acq = starpu_data_acquire(data_handles[0][lx][ly], STARPU_R);
			matrix = (float*) matrixi->ptr;
			for (int xx = 0; xx < matrixi->nx; xx++){
				printf("[ ");
				for (int yy = 0; yy < matrixi->ny; yy++){
					printf("%.2f ", matrix[xx*matrixi->ld+yy]);
				}
				printf("]\n");
			}
			starpu_data_release(data_handles[0][lx][ly]);
		}
		if (matrixi->ny > 1) {
			printf("north data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][NORTH], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d ; offset %d\n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize, matrixi->offset);
			if (me == cells[0][lx][ly].owner) {
				acq = starpu_data_acquire(children_handles[0][lx][ly][NORTH], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][NORTH]);
			}
			printf("south data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][SOUTH], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d ; offset %d\n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize, matrixi->offset);
			if (me == cells[0][lx][ly].owner) {
				acq = starpu_data_acquire(children_handles[0][lx][ly][SOUTH], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][SOUTH]);
			}
		}
		if (matrixi->nx>1) {
			printf("west data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][WEST], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d ; offset %d\n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize, matrixi->offset);
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][WEST], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][WEST]);
			}
			printf("east data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][EAST], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d \n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize);
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][EAST], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][EAST]);
			}
		}
		if (replicated_variant && !is_frontier(lx,ly))
		{
			printf("north west data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][NOWE], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d \n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize);
			
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][NOWE], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][NOWE]);
			}
			printf("north east data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][NOEA], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d \n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize);
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][NOEA], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][NOEA]);
			}
			printf("south west data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][SOWE], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d \n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize);
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][SOWE], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][SOWE]);
			}
			printf("south east data\n");
			matrixi = starpu_data_get_interface_on_node(children_handles[0][lx][ly][SOEA], STARPU_MAIN_RAM);
			printf("random %d info %p XxY_LD = %dx%d_%d ; elem size %d ; alloc size %d \n",acq,matrixi->ptr, matrixi->nx,matrixi->ny,matrixi->ld, matrixi->elemsize, matrixi->allocsize);
			if (me == cells[0][lx][ly].owner){
				acq = starpu_data_acquire(children_handles[0][lx][ly][SOEA], STARPU_R);
				matrix = (float*) matrixi->ptr;
				for (int xx = 0; xx < matrixi->nx; xx++){
					printf("[ ");
					for (int yy = 0; yy < matrixi->ny; yy++){
						printf("%.2f ", matrix[xx*matrixi->ld+yy]);
					}
					printf("]\n");
				}
				starpu_data_release(children_handles[0][lx][ly][SOEA]);
			}
		}

		for (y = 0; y < Y; y++)
		{
			fprintf(stdout, "(%d) [ ", x);
			for(x = 0; x < X; x++)
			{
				if (replicated_variant)
				{
					matrixi = starpu_data_get_interface_on_node(data_handles[0][x][y], STARPU_MAIN_RAM);
					int ny = matrixi->ny;
					int nx = matrixi->nx;
					fprintf(stdout, "(%d,%d) %d - %dx%d", x,y, cells[0][x][y].owner, nx, ny);
					if (ny > 1)
					{
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][NORTH], STARPU_MAIN_RAM);
						fprintf(stdout, " - N %dx%d", matrixi->nx, matrixi->ny);
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][SOUTH], STARPU_MAIN_RAM);
						fprintf(stdout, " - S %dx%d", matrixi->nx, matrixi->ny);
					}
					if (nx > 1)
					{
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][EAST], STARPU_MAIN_RAM);
						fprintf(stdout, " - E %dx%d", matrixi->nx, matrixi->ny);
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][WEST], STARPU_MAIN_RAM);
						fprintf(stdout, " - W %dx%d", matrixi->nx, matrixi->ny);
					}
					if (!is_frontier(x,y))
					{
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][NOEA], STARPU_MAIN_RAM);
						fprintf(stdout, " - NE %dx%d", matrixi->nx, matrixi->ny);
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][SOEA], STARPU_MAIN_RAM);
						fprintf(stdout, " - SE %dx%d", matrixi->nx, matrixi->ny);
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][NOWE], STARPU_MAIN_RAM);
						fprintf(stdout, " - NW %dx%d", matrixi->nx, matrixi->ny);
						matrixi = starpu_data_get_interface_on_node(children_handles[0][x][y][SOWE], STARPU_MAIN_RAM);
						fprintf(stdout, " - SW %dx%d", matrixi->nx, matrixi->ny);
					}
				}
				fprintf(stdout, ", ");
			}
			fprintf(stdout, "]\n");
		}
		for (int i = 0; i < niter; i++)
		{
			int current_ring = s - i % (s+1);
			int ring_size = current_ring == 0 ? 0 : (current_ring-1)/b+1;
			int write_ring = (loop-1+s)%s; //# + 1;
			int read_ring = write_ring + 1;
			int write_size = write_ring == 0 ? 0 : (write_ring - 1)/ b + 1;
			int read_size = (read_ring - 1)/ b + 1;
//			fprintf(stdout," iter = %d (%d)\n", i, current_ring);
			fprintf(stdout, "for 1, iter %d read_size %d write_size %d\n", i, read_size, write_size);
			for (y = 0; y < Y; y++)
			{
				for(x = 0; x < X; x++)
				{
					int nb_nodes;
					int *nodes = nodes_around(cells,x,y,read_size,&nb_nodes);
					
					fprintf(stdout, "["); //(%d) ", cells[0][x][y].owner);
					for (int nn = 0; nn < nb_nodes; nn++)
					{
						fprintf(stdout, "%d, ", nodes[nn]);
					}
					fprintf(stdout, "], ");
					
				}
				fprintf(stdout, "]\n");
			}
			for (int node = 0; node < size; node++) {
				starpu_mpi_barrier(MPI_COMM_WORLD);
				if (me == node) {
					fprintf(stdout, "for %d, iter %d read_size %d write_size %d\n", me, i, read_size, write_size);
					for (y = 0; y < Y; y++)
					{
						fprintf(stdout, "["); //(%d) ", cells[0][x][y].owner);
						for(x = 0; x < X; x++)
						{
							int northern = node_around(cells,x,y,read_size,me);
							fprintf(stdout, "%d ", northern);
						}
						fprintf(stdout, "]\n");
					}
					fprintf(stdout, "for %d, iter %d exact read_size %d write_size %d\n", me, i, read_size, write_size);
					for (y = 0; y < Y; y++)
					{
						for(x = 0; x < X; x++)
						{
							int northern = node_around_exact(cells,x,y,read_size,me);
							fprintf(stdout, "%d ", northern);
						}
						fprintf(stdout, "]\n");
					}
				}
				starpu_mpi_barrier(MPI_COMM_WORLD);
			}
		}
	}
	starpu_mpi_wait_for_all(MPI_COMM_WORLD);
	if (debug)
		fprintf(stdout,"[%d] Measuring time ... \n", me);

	for (int i = 0; i < trial; i ++)
	{
		starpu_mpi_barrier(MPI_COMM_WORLD);
		double start = what_time_is_it();
	
//		fprintf(stdout,"[%d] Submitting DAG\n", me);
		/* submitting DAG */
		if (replicated_variant)
		{
			submit_replicated_variant(cells, dummy_handle, data_handles, children_handles);
		}
		else
		{
			submit_classical_variant(cells, data_handles, children_handles);
		}
//		fprintf(stdout,"[%d] DAG submitted\n", me);
	
		starpu_mpi_wait_for_all(MPI_COMM_WORLD);
		starpu_mpi_barrier(MPI_COMM_WORLD);
		double end = what_time_is_it();
		if (me == 0)
			fprintf(stdout,"[%d] took %f seconds\n", me, end-start);
	}

	starpu_data_handle_t tmp_handles[4]; // at most
	for(x = 0; x < X; x++)
	{
		for (y = 0; y < Y; y++)
		{
			int nx = x == 0 || x == X-1 ? 1 : b;
			int ny = y == 0 || y == Y-1 ? 1 : b;
			for(int grid=0; grid<S;grid++) {
				if (data_handles[grid][x][y])
				{
 					if (!replicated_variant)
					{
						if (me == cells[0][x][y].owner)
						{
							if (ny > 1)
							{
								tmp_handles[2] = children_handles[grid][x][y][NORTH];
								tmp_handles[1] = children_handles[grid][x][y][CENTER_BIS];
								tmp_handles[0] = children_handles[grid][x][y][SOUTH];
								starpu_data_partition_clean(data_handles[grid][x][y], 3, tmp_handles);
      							}
      							if (nx > 1)
      							{
								tmp_handles[2] = children_handles[grid][x][y][WEST];
								tmp_handles[1] = children_handles[grid][x][y][CENTER];
								tmp_handles[0] = children_handles[grid][x][y][EAST];
								starpu_data_partition_clean(data_handles[grid][x][y], 3, tmp_handles);
      							}
      						}
						// unregistering children_handles for "others" segfault, idk y & idc
						starpu_data_unregister(data_handles[grid][x][y]);
					}
					else
					{
						if (me == cells[0][x][y].owner)
						{
							if (ny > 1)
							{
								tmp_handles[0] = children_handles[grid][x][y][NORTH];
								tmp_handles[1] = children_handles[grid][x][y][SOUTH];
								starpu_data_partition_clean(data_handles[grid][x][y], 2, tmp_handles);
							}
							if (nx > 1)
							{
								tmp_handles[0] = children_handles[grid][x][y][WEST];
								tmp_handles[1] = children_handles[grid][x][y][EAST];
								starpu_data_partition_clean(data_handles[grid][x][y], 2, tmp_handles);
							}
							if (!is_frontier(x,y))
							{
								tmp_handles[0] = children_handles[grid][x][y][NOEA];
								tmp_handles[1] = children_handles[grid][x][y][NOWE];
								tmp_handles[2] = children_handles[grid][x][y][SOEA];
								tmp_handles[3] = children_handles[grid][x][y][SOWE];
								starpu_data_partition_clean(data_handles[grid][x][y], 4, tmp_handles);
      							}
      						}
						// unregistering children_handles for "others" segfault, idk y & idc
						starpu_data_unregister(data_handles[grid][x][y]);
					}
				}
			}
		}
	}
	if (prof)
	{
		starpu_codelet_display_stats(&fill_cl);
		starpu_mpi_barrier(MPI_COMM_WORLD);
		if (replicated_variant)
			starpu_codelet_display_stats(&stencil_repl_cl);
		else
			starpu_codelet_display_stats(&stencil_cl);
	}
	starpu_mpi_barrier(MPI_COMM_WORLD);
	// free cells
	for (int grid = 0 ; grid < S; grid++) {
		for (x = 0 ; x < X; x++) {
			for (y = 0 ; y < Y; y++) {
				if (me == cells[0][x][y].owner)
				{
//					free(cells[grid][x][y].center); // CAN CAUSE SEGFAULT IDK WHY & IDC
				}
			}
			free(cells[grid][x]);
		}
		free(cells[grid]);
	}
	free(cells);
	// free handles
	for (int grid = 0 ; grid < S; grid++) {
		for (x = 0 ; x < X; x++) {
			for (y = 0 ; y < Y; y++) {
				// already null after cleaning free(children_handles[grid][x][y]);
			}
			free(data_handles[grid][x]);
			free(children_handles[grid][x]);
		}
		free(data_handles[grid]);
		free(children_handles[grid]);
	}
	free(data_handles);
	free(children_handles);

	starpu_mpi_shutdown();

	return 0;
}
